<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

$MESS['RETOR_BONUS_OPTIONS'] = "Общие настройки";
$MESS['RETOR_BONUS_RULES'] = "Правила при оплате";
$MESS['RETOR_BONUS_PROGRAMM'] = "Программы лояльности";
$MESS['RETOR_BONUS_KUPONS'] = "Купоны";
$MESS['RETOR_BONUS_ADITINAL_OPTIONS'] = "Дополнительные настройки";

$MESS['RETOR_BONUS_USE_DISCOUNT'] = "Применять скидку";
$MESS['RETOR_BONUS_NOT_DISCOUNTED'] = "Не применять к акционным";
$MESS['RETOR_BONUS_MAX_PERCENT'] = "Максимальный % оплаты бонусами от суммы заказа";
$MESS['RETOR_BONUS_PAY_SYSTEM_ID'] = "ID платежной системы";

$MESS['RETOR_BONUS_SEND_ADMIN'] = "Отправить письмо администратору о (начислении/списании) бонусов";
$MESS['RETOR_BONUS_SEND_USER'] = "Отправить письмо пользователю о (начислении/списании) бонусов";

$MESS['RETOR_BONUS_ON_ORDER'] = "На заказ";
$MESS['RETOR_BONUS_ON_GOODS'] = "На товары";
$MESS['RETOR_BONUS_AT_USERS_GROUP'] = "На группу пользователей";
$MESS['RETOR_BONUS_USERS_GROUPS'] = "Группы пользователей";
$MESS['RETOR_BONUS_ACCRUAL_PERCENT'] = "Процент начисления (%)";
$MESS['RETOR_BONUS_FIX_SUMM'] = "Фиксированная сумма (руб.)";
$MESS['RETOR_BONUS_ACCRUAL_VAL'] = "Величина начисления";
$MESS['RETOR_BONUS_MIN_ORDER_SUMM'] = "Минимальная сумма заказа";
$MESS['RETOR_BONUS_EVENT'] = "Событие";
$MESS['RETOR_BONUS_FIRST_BUY'] = "Бонус при первой покупке";
$MESS['RETOR_BONUS_CUPUN_TIME'] = "Время жизни купленого купона";
$MESS['RETOR_BONUS_PER_DAY'] = "Значение (в днях)";
$MESS['RETOR_BONUS_COUPON_NOM'] = "Номиналы купонов";
$MESS['RETOR_BONUS_IGNORE_ORDERS'] = "Игнорировать заказы до номера";

$MESS['RETOR_BONUS_ACTIVE'] = "Активен";
$MESS['RETOR_BONUS_SITE'] = "Сайт";

$MESS['RETOR_BONUS_'] = "К заказу";
$MESS['RETOR_BONUS_'] = "К товарам в заказе";
$MESS['RETOR_BONUS_'] = "При оформлении заказа скидка не будет применена к товарам со скидкой";
$MESS['RETOR_BONUS_'] = "Суммарная скидка на (товар/заказ) не может привышать это значение";
$MESS['RETOR_BONUS_'] = "Сумма ниже которой процент начислен не будет, если 0 - то начисляем с любой";
$MESS['RETOR_BONUS_'] = "Перевод в статус Выполнен";
$MESS['RETOR_BONUS_'] = "После оплаты заказа";
$MESS['RETOR_BONUS_'] = "Бессрочный";
$MESS['RETOR_BONUS_'] = "Кол-во дней";
$MESS['RETOR_BONUS_'] = "Через запятую";
$MESS['RETOR_BONUS_'] = "Заказы с номером мешье не будут учтены в бонусной программе";


$MESS['RETOR_BONUS_OPTIONS_SAVED'] = "Настройки успешно сохранены!";
$MESS['RETOR_BONUS_INVALID_VALUE'] = "Ошибка сохранения настроек";