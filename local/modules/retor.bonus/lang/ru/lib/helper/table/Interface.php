<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 15.07.17
 * Time: 20:15
 */

$MESS['ACCOUNTS_TABLE_ENTITY_ID_FIELD'] = 'ID';
$MESS['ACCOUNTS_TABLE_ENTITY_USER_GROUP_ID_FIELD'] = 'Группа пользователя';
$MESS['ACCOUNTS_TABLE_ENTITY_USER_ID_FIELD'] = 'Пользователь';
$MESS['ACCOUNTS_TABLE_ENTITY_PERCENT_FIELD'] = 'Процент';
$MESS['ACCOUNTS_TABLE_ENTITY_BONUS_COUNT_FIELD'] = 'Бонусные баллы';
$MESS['ACCOUNTS_TABLE_ENTITY_SALE_AMOUNT_FIELD'] = 'Сумма заказов';
$MESS['ACCOUNTS_TABLE_ENTITY_MODIFIED_FIELD'] = 'Дата изменений';
