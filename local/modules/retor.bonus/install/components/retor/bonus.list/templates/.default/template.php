<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<section class="container_">
    <h1 class="title-history">Мой заработок</h1>
    <div class="total-stored">
        Накоплено итого: <?= ($arResult['BONUS_COUNT'] > 0) ? $arResult['BONUS_COUNT'] : '0'; ?> рублей
    </div>
    <div class="stored-history">
        <div class="stored-history-title">История начислений:</div>
        <ul class="preference-block">
            <? foreach ($arResult['TRANSACT'] as $item): ?>
                <li class="preference-item"><?= $item['TRANSACT_DATE']; ?> Заказ № <?= $item['ORDER_ID']; ?> – сумма
                    заказа <?= round($item['ORDER_SUM']); ?>
                    р., <?= ($item['DESCRIPTION'] == 'DEBET') ? 'списано ' : 'начислено '; ?>
                    <span <?= ($item['DESCRIPTION'] == 'DEBET') ? 'style="color:red;"' : ''; ?>><?= ($item['DESCRIPTION'] == 'DEBET') ? '- ' : '+ '; ?><?= round($item['BONUS']); ?></span>
                    р.
                </li>
            <? endforeach; ?>
        </ul>
    </div>
</section>