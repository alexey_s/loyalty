<?php

namespace retor\bonus\Helper\table;

use retor\bonus\Helper\AdminEditHelper;

class TableEditHelper extends AdminEditHelper
{
    static protected $model = 'retor\bonus\Model\AccountsTable';
    static public $module = 'retor.bonus';
    static protected $listViewName = 'table_list';
    static protected $viewName = 'table_detail';
}

