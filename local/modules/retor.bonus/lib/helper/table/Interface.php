<?php

use Bitrix\Main\Localization\Loc;
use retor\bonus\Helper\AdminBaseHelper;
use retor\bonus\Widget\NumberWidget;
use retor\bonus\Widget\DateTimeWidget;
use retor\bonus\Widget\UserWidget;

Loc::loadMessages(__FILE__);

AdminBaseHelper::setInterfaceSettings(
    array(
        'FIELDS' => array(
            'ID' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_ID_FIELD'),
            ),
            'USER_ID' => array(
                'WIDGET' => new UserWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_USER_ID_FIELD'),
            ),
            'USER_GROUP_ID' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_USER_GROUP_ID_FIELD'),
            ),
            'PERCENT' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_PERCENT_FIELD'),
            ),
            'BONUS_COUNT' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_BONUS_COUNT_FIELD'),
            ),
            'SALE_AMOUNT' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_SALE_AMOUNT_FIELD'),
            ),
            'MODIFIED' => array(
                'WIDGET' => new DateTimeWidget(),
                'TITLE' => Loc::getMessage('ACCOUNTS_TABLE_ENTITY_MODIFIED_FIELD'),
            )
        ),
    ),
    array(
        'retor\bonus\Helper\table\TableEditHelper',
        'retor\bonus\Helper\table\TableListHelper'
    ),
    'retor.bonus'
);




