<?php

use Bitrix\Main\Localization\Loc;
use retor\bonus\Helper\AdminBaseHelper;
use retor\bonus\Widget\NumberWidget;
use retor\bonus\Widget\ListWidget;
use retor\bonus\Widget\CheckboxWidget;
use retor\bonus\Widget\StringWidget;
use retor\bonus\Widget\UserGroupWidget;
use retor\bonus\Widget\SiteWidget;

Loc::loadMessages(__FILE__);

AdminBaseHelper::setInterfaceSettings(
    array(
        'FIELDS' => array(
            'ID' => array(
                'WIDGET' => new NumberWidget(),
                'TITLE' => Loc::getMessage('PROGRAM_TABLE_ENTITY_ID_FIELD'),
            ),
            'ACTIVE' => array(
                'WIDGET' => new CheckboxWidget(),
                'TITLE' => Loc::getMessage('PROGRAM_TABLE_ENTITY_ACTIVE_FIELD'),
            ),
            'PROGRAM_NAME' => array(
                'WIDGET' => new StringWidget(),
                'TITLE' => Loc::getMessage('PROGRAM_TABLE_ENTITY_PROGRAM_NAME_FIELD'),
            ),
            'SITE_ID' => array(
                'WIDGET' => new SiteWidget(),
                'TITLE' => Loc::getMessage('PROGRAM_TABLE_ENTITY_SITE_FIELD'),
            ),
            'USER_GROUP_ID' => array(
                'WIDGET' => new UserGroupWidget(),
                'TITLE' => Loc::getMessage('PROGRAM_TABLE_ENTITY_USER_GROUP_ID_FIELD'),
            ),
            'PERCENTS' => array(
                'WIDGET' => new ListWidget(),
                'TITLE' => Loc::getMessage('PROGRAM_TABLE_ENTITY_PERCENT_FIELD'),
            ),
            'SUM' => array(
                'WIDGET' => new StringWidget(),
                'VIRTUAL' => true,
                'LIST' => false,
               // 'READONLY' => true
            ),
            'TYPE' => array(
                'WIDGET' => new StringWidget(),
                'VIRTUAL' => true,
                'LIST' => false,
              //  'READONLY' => true
            )
        )
    ),
    array(
        'retor\bonus\Helper\program\ProgramEditHelper',
        'retor\bonus\Helper\program\ProgramListHelper'
    ),
    'retor.bonus'
);