<?php

namespace retor\bonus\Widget;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Виджет с числовыми значениями. Точная копия StringWidget, только работает с числами и не ищет по подстроке.
 */
class UserGroupWidget extends HelperWidget
{
    protected function getEditHtml()
    {
        $arGroupList = array();
        $arGroupList[] = unserialize($this->getValue());
        $arGroupList = $arGroupList[0];



        $table = '<tr id="tr_GROUP_IDS" class="adm-detail-required-field">
		          <td valign="top" width="40%"></td>
                      <td width="60%" align="left">
                            <select name="USER_GROUP_ID[]" multiple size="8">';
                                $rsUserGroups = \CGroup::GetList(($by = 'c_sort'), ($order = "asc"), array(), "N");
                                while ($arUserGroup = $rsUserGroups->Fetch()) {
                                    if (2 != $arUserGroup['ID']) {
                                        $table .= '<option value="' . intval($arUserGroup['ID']) .'" '. (in_array($arUserGroup['ID'], $arGroupList) ? "selected" : "") . '>' . htmlspecialcharsex($arUserGroup['NAME']) . '</option>';
                                    }
                                }

        $table .= '</select></td></tr>';

        return $table;

    }

    public function processEditAction()
    {

       $this->setValue(serialize($this->data['USER_GROUP_ID']));
       parent::processEditAction();

    }

    public function generateRow(&$row, $data)
    {
        $groups = '';
        $arGroupList = unserialize($data[$this->code]);

        foreach($arGroupList as $group){
            $groups.= $group." | ";
        }



        $filter = Array
        (
            "ID"             =>  $groups,
            "ACTIVE"         => "Y",
        );

        $html = '';
        $rsUserGroups = \CGroup::GetList(($by = 'c_sort'), ($order = "asc"), $filter, "N");
        while ($arUserGroup = $rsUserGroups->Fetch()) {
            $html .= $arUserGroup['NAME'].'<br>';
        }

        $row->AddViewField($this->code, $html);
    }

    public function showFilterHtml()
    {

    }

}
