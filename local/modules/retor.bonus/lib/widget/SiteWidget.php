<?php
/**
 * Created by PhpStorm.
 * User: devaccess
 * Date: 19.07.17
 * Time: 21:13
 */

namespace retor\bonus\Widget;

use Bitrix\Main\Localization\Loc;
use retor\bonus\Helper\AdminEditHelper;
use retor\bonus\Helper\AdminListHelper;
use retor\bonus\Helper\AdminSectionListHelper;

Loc::loadMessages(__FILE__);

class SiteWidget extends HelperWidget
{


    protected function AddDropDownField($id, $arSelect, $value=false, $arParams=array())
    {
        if(!$value){$value = 0;}
        $html = '<select name="'.$id.'"';
        foreach($arParams as $param)
            $html .= ' '.$param;
        $html .= '>';

        foreach($arSelect as $key => $val)
            $html .= '<option value="'.htmlspecialcharsbx($key).'"'.($value == $key? ' selected': '').'>'.htmlspecialcharsex($val).'</option>';
        $html .= '</select>';

        return $html;

    }

    protected function getEditHtml()
    {

        $arSiteList = array();
        $by = 'sort';
        $order = 'asc';
        $rsSites = \CSite::GetList($by, $order);
        while ($arSite = $rsSites->Fetch())
        {
            $arSiteList[$arSite['LID']] = '('.$arSite['LID'].') '.$arSite['NAME'];
        }

        return self::AddDropDownField('SITE_ID',$arSiteList,$this->getValue());

    }

    public function generateRow(&$row, $data)
    {

    }

    public function showFilterHtml()
    {

    }

}

?>