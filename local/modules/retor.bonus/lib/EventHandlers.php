<?php

namespace retor\bonus;

use Bitrix\Main\Context;
use Bitrix\Main\Loader;


/**
 * Перехватчики событий.
 *
 * Для каждого события, возникающего в системе, которе необходимо отлавливать «Админ-хелпером», создаётся
 * в данном классе одноимённый метод. Метод должен быть зарегистрирован в системе через установщик модуля.
 *
 * @author Nik Samokhvalov <nik@samokhvalov.info>
 */
class EventHandlers
{
    /**
     * Автоматическое подключение модуля в админке.
     *
     * Таки образом, исключаем необходимость прописывать в генераторах админки своих модулей
     * подключение «Админ-хелпера».
     *
     * @throws \Bitrix\Main\LoaderException
     */
    public static function onPageStart()
    {
        if (Context::getCurrent()->getRequest()->isAdminSection()) {
            Loader::includeModule('retor.bonus');
        }
    }

    public static function OnSaleStatusOrder($ID, $val)
    {

        //Получаем заказ
        $arOrder = \CSaleOrder::GetByID($ID);
        //

        $BonusProgram = \CBonusAccount::GetBonusSettings($arOrder['USER_ID'],$arOrder['LID']);

        if ($arOrder['LID'] == $BonusProgram['site_id'] && $BonusProgram['bonus_active'] == "Y" && !empty($BonusProgram['programs'])) {

            //Отправляем в обработку

            if ($arOrder) {

                $Order = array(
                    'ID' => $arOrder['ID'],
                    'LID' => $arOrder['LID'],
                    'STATUS_ID' => $arOrder['STATUS_ID'],
                    'DATE_INSERT' => $arOrder['DATE_INSERT'],
                    'DATE_UPDATE' => $arOrder['DATE_UPDATE'],
                    'PRICE' => $arOrder['PRICE']
                );

                $arFields = array(
                    'USER_ID' => $arOrder['USER_ID'],
                    'OrderStatus' => $val,
                    'Order' => $Order,
                    'BonusProgram' => $BonusProgram['programs']
                );

                \CBonusAccount::PayIn($arFields);

            }

        }

    }

    public static function OnSaleComponentOrderResultPrepared($order, &$arUserResult, $request, &$arParams, &$arResult)
    {
        return $arResult;
    }

    public static function OnSaleOrderSaved($event)
    {
        $order = $event->getParameter("ENTITY");
        //\Bitrix\Sale\Order::getList();
        file_put_contents('/tmp/save.log',print_r($order->getId(),true),FILE_APPEND);
        // file_put_contents('/tmp/save.log',print_r($_REQUEST,true),FILE_APPEND);
        var_dump($event); exit;
    }
}