<?php

namespace retor\bonus\Model;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class AccountsTable extends DataManager
{
    public static function getTableName()
    {
        return 'retor_bonus_accounts_table';
    }

    public static function getMap()
    {
        $fieldsMap = array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ),
            'USER_ID' => array(
                'data_type' => 'integer',
            ),
            'USER_GROUP_ID' => array(
                'data_type' => 'integer',
            ),
            'PERCENT' => array(
                'data_type' => 'integer',
            ),
            'BONUS_COUNT' => array(
                'data_type' => 'integer',
            ),
            'SALE_AMOUNT' => array(
                'data_type' => 'integer',
            ),
            'MODIFIED' => array(
                'data_type' => 'datetime',
            ),
        );

        return $fieldsMap;
    }
}