<?php

namespace retor\bonus\Model;


use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class ProgramTable extends DataManager
{
    public static function getTableName()
    {
        return 'retor_bonus_program_table';
    }

    public static function getMap()
    {
        $fieldsMap = array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('PROGRAM_TABLE_ENTITY_ID_FIELD'),
            ),
            'ACTIVE' => array(
                'data_type' => 'string',
                'required' => true,
                'title' => Loc::getMessage('PROGRAM_TABLE_ENTITY_ACTIVE_FIELD'),
            ),
            'PROGRAM_NAME' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('PROGRAM_TABLE_ENTITY_PROGRAM_NAME_FIELD'),
            ),
            'SITE_ID' => array(
                'data_type' => 'string',
                'required' => true,
                'title' => Loc::getMessage('PROGRAM_TABLE_ENTITY_SITE_ID_FIELD'),
            ),
            'USER_GROUP_ID' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('PROGRAM_TABLE_ENTITY_USER_GROUP_ID_FIELD'),
            ),
            'PERCENTS' => array(
                'data_type' => 'string',
                'title' => Loc::getMessage('PROGRAM_TABLE_ENTITY_PERCENTS_FIELD'),
            )
        );

        return $fieldsMap;
    }
}